import { Base } from "./base";
import { Entry } from "./entry";

/**
 * A generic Person object - can be either the Canvasser (USER) or the person
 * being contacted (CONTACT), as determined by the 'role' field.
 */

export enum PersonRole {
  USER = "USER",
  CONTACT = "CONTACT",
}

export interface Person extends Base {
  firstName: string;
  lastName: string;
  email: string;
  role: PersonRole;
  contactsMade: Entry[];
  contactsReceived: Entry[];
}
