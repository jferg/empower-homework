export interface Base {
  id: string;
  createTimestamp: Date;
  modifyTimestamp: Date;
}
