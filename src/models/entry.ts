import { Person } from "./person";
import { Base } from "./base";

export interface Entry extends Base {
  /**
   * The content of the entry.
   */
  content: string;

  // Relations
  user: Person;
  contact: Person;
}
