import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import Home from "../views/Home.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/view-notes",
    name: "View Notes",
    // route level code-splitting
    // this generates a separate chunk (viewnotes.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import("../views/ViewNotes.vue"),
  },
  {
    path: "/log-contact",
    name: "Log a Contact",
    component: () => import("../views/LogContact.vue"),
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
