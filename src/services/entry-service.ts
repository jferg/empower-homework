import { BaseService } from "@/services/base-service";
import { Entry } from "@/models/entry";

export class EntryService extends BaseService<Entry> {
  constructor(baseUrl: string) {
    super(baseUrl, "entry");
  }
}
