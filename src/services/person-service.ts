import { BaseService } from "./base-service";
import { Person } from "@/models/person";

export class PersonService extends BaseService<Person> {
  constructor(baseUrl: string) {
    super(baseUrl, "person");
  }
}
