export * from "./entry-service";
export * from "./person-service";

export enum ServiceIdentifiers {
  "ENTRY_SERVICE" = "entryService",
  "PERSON_SERVICE" = "personService",
}
