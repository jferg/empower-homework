import { Person } from "@/models/person";
import { Store } from "@/store/base-store";

export interface Persons {
  items: Array<Person>;
}

/**
 * Abstract extension of Base Store class for storing persons.
 */
export class PersonStore extends Store<Persons> {
  protected data(): Persons {
    return {
      items: [],
    };
  }

  public setPersons(newPersons: Array<Person>): Array<Person> {
    this.state.items.splice(0, this.state.items.length, ...newPersons);
    return this.state.items;
  }
}
