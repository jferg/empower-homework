import { Store } from "@/store/base-store";
import { Entry } from "@/models/entry";

interface Entries {
  items: Array<Entry>;
}

/**
 * Implementation of Base Store object for storing Entry objects.
 */
export class EntriesStore extends Store<Entries> {
  protected data(): Entries {
    return {
      items: [],
    };
  }

  public setEntries(newEntries: Array<Entry>): Array<Entry> {
    if (Array.isArray(newEntries)) {
      this.state.items.splice(0, this.state.items.length, ...newEntries);
    } else {
      this.state.items.splice(0, this.state.items.length);
    }
    return this.state.items;
  }
}

export const entriesStore = new EntriesStore("ENTRIES");
