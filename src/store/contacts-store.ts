import { PersonStore } from "@/store/person-store";

/**
 * Implementation of Base Store class for Contacts.  Stores a list of
 * Person objects to be accessed by components.
 */
export class ContactsStore extends PersonStore {}
export const contactsStore = new ContactsStore("CONTACTS");
