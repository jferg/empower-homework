import request from 'supertest';
import { App } from '../src/app';
import {createConnection, getConnectionOptions} from 'typeorm';

let app: App;

beforeAll(async () => {
  const connectionOptions = await getConnectionOptions();
  Object.assign(connectionOptions, {database: 'test-db.sqlite3'});
  app = new App({port: 3838, dbConnection: await createConnection(connectionOptions)});
  app.init();
  app.run();
  return;
})

afterAll(async () => {
  app.httpServer.close();
  return;
});

describe('basic route test', () => {
  test('get home route GET /', async () => {
    const response = await request(app.httpServer).get('/');
    expect(response.status).toBe(404);
    return;
  })
})