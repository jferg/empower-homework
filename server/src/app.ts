import "reflect-metadata";
import { Connection, createConnection } from "typeorm";
import Koa from "koa";
import koaStatic from "koa-static-server";
import { Server as HttpServer } from "http";
import { BaseEndpoint } from "./endpoints/baseEndpoint";
import { EntryEntity, PersonEntity } from "./entities";
import { Entry } from "./models/entry";
import { Person } from "./models/person";

export class App {
  private readonly port: number = 3000;
  private koaApp!: Koa;
  private db!: Connection | undefined;

  // We expose the httpServer only for testing purposes.
  public httpServer!: HttpServer;

  /**
   * Sets up the basic things needed to spin up the app.
   *
   * @param port The port to listen on - defaults to 3000.
   * @param dbConnection The connection to the database.
   */
  constructor(
    { port, dbConnection }: { port: number; dbConnection?: Connection } = {
      port: 3000,
    }
  ) {
    this.db = dbConnection;
    if (port) {
      this.port = port;
    }
  }

  /**
   * Creates our base Koa app, and registers our restful endpoints with the app.
   * Registers the static server to serve the Vue app.
   */
  public init() {
    this.koaApp = new Koa();

    const entryEndpoint = new BaseEndpoint<Entry>(EntryEntity, "entry");
    const personEndpoint = new BaseEndpoint<Person>(PersonEntity, "person");

    entryEndpoint.registerEndpoint(this.koaApp);
    personEndpoint.registerEndpoint(this.koaApp);

    this.koaApp.use(
      koaStatic({ rootDir: "./dist", notFoundFile: "index.html" })
    );
  }

  /**
   * Starts the Koa app.
   */
  public run() {
    console.log(`Starting listener on port ${this.port}.`);
    console.log(`Visit http://localhost:${this.port}/ to use the app!`);
    this.httpServer = this.koaApp.listen(this.port);
  }
}

/**
 * If we're not running as part of a test, start up the app.
 */
if (process.env.NODE_ENV !== "test") {
  (async () => {
    const app = new App({
      port: parseInt(process.env.PORT ?? "3000"),
      dbConnection: await createConnection(),
    });
    try {
      app.init();
      app.run();
    } catch (e) {
      console.error("Failed to start server: ", e);
    }
  })();
}
