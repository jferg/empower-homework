import { Entry } from "../models/entry";
import { EntitySchema } from "typeorm";
import { BaseEntityColumns } from "./base-entity";

export const EntryEntity = new EntitySchema<Entry>({
  name: "entry",

  columns: {
    ...BaseEntityColumns,
    content: {
      type: "text",
    },
  },

  relations: {
    user: {
      eager: true,
      type: "many-to-one",
      target: "person",
      cascade: true,
      inverseSide: "contactsMade",
    },
    contact: {
      eager: true,
      type: "many-to-one",
      target: "person",
      cascade: true,
      inverseSide: "contactsReceived",
    },
  },
});
