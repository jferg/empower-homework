import { EntitySchemaColumnOptions } from "typeorm";

/**
 * Base column descriptors that all entities should have.
 *    * ID (uuid type)
 *    * createTimestamp (Date type)
 *    * modifyTimestamp (Date type)
 */
export const BaseEntityColumns = {
  id: {
    type: "varchar",
    primary: true,
    generate: "uuid",
    nullable: true,
  } as EntitySchemaColumnOptions,
  createTimestamp: {
    createDate: true,
    type: "date",
  } as EntitySchemaColumnOptions,
  modifyTimestamp: {
    updateDate: true,
    type: "date",
  } as EntitySchemaColumnOptions,
};
