# empower-homework

This is a relatively simplistic implementation of a canvassing notes application.  It makes
use of Vue.JS 3, Quasar 2, TypeORM, and SQLite3 for the database backend, for simplicity's 
sake.  This is by no means production-ready code, but it should provide minimal functionality
which is usable.  

The reasoning behind the choices of toolsets is primarily that I've been working with Vue.JS 2 and Quasar 1.x, and this was a good opportunity to try out some of the new Vue 3 changes, and also to create what is basically a nice app boilerplate that I can probably re-use for other projects.  

The list of things I'd want to do before actually deploying is long - these are some of them.

* Some unit and integration tests have been implemented on the server side; ideally those would be fleshed out and there would be equivalent tests on the front end. 
* There is no access control or authentication right now.  
* There is a simple vector for cross-site scripting in the NotesLog vue component.  
* There is much room for improvement in the UI.
* There is little to no error handling in either frontend _or_ backend, friendly or otherwise.  
* Docker setup is pretty minimal, backend should be compiled to JS instead of using `ts-node`, as `ts-node` has memory leaks.
* The use of sqlite obviously is not ideal for production, the typeorm configuration should be easily switched to using a network-accessible database.
* It's not ideal to be using the Koa app to serve the Vue static HTML - there should be a real nginx instance or similar in front serving the static content.

## Local Startup
```
npm install
npm run build
cd server
npm install
npm start
```

## Development Mode Startup

Both pieces need to be running for the app to work.  

### Frontend
```
npm install
npm run serve
```
### Backend
```
cd server
npm install
npm run dev
```

The application will be accessible at http://localhost:8080/.

## Docker
```
docker build . -t canvass-notes
docker run -p 3000:3000 canvass-notes
```

The application will be accessible at http://localhost:3000/.

### Run tests
```
cd server
npm test
```

(There currently are no frontend tests.)

