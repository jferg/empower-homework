FROM node:14-alpine AS build-stage

WORKDIR /usr/src/app

COPY package*.json ./
RUN npm install
COPY ./src ./src/
COPY ./public ./public/
COPY ./tsconfig.json ./
COPY ./vue.config.js ./
COPY ./babel.config.js ./
RUN npm run build

FROM node:14-alpine AS deploy-stage
WORKDIR /usr/src/app
COPY server/package*.json ./
RUN npm install
COPY server/src ./src/
COPY server/typings ./typings/
COPY src/models ./src/models
COPY server/jest.config.js ./
COPY server/ormconfig.json ./
COPY server/tsconfig.json ./
COPY --from=build-stage /usr/src/app/dist /usr/src/app/dist

ENV PORT=3000

EXPOSE 3000

CMD [ "npm", "start" ]
